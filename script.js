document.addEventListener('DOMContentLoaded', function() {
    const scrollToTopBtn = document.getElementById('scrollToTopBtn');

    // Afficher le bouton lorsque l'utilisateur a fait défiler la moitié de la hauteur de la fenêtre
    window.onscroll = function() {
        if (document.body.scrollTop > window.innerHeight / 2 || document.documentElement.scrollTop > window.innerHeight / 2) {
            scrollToTopBtn.style.display = 'block';
            scrollToTopBtn.style.opacity = '1';
        } else {
            scrollToTopBtn.style.opacity = '0';
            // Attendre 300 ms avant de cacher le bouton pour une animation plus douce
            setTimeout(function() {
                scrollToTopBtn.style.display = 'none';
            }, 100);
        }
    };

    // Faire défiler en haut de la page avec une animation douce lorsque le bouton est cliqué
    scrollToTopBtn.addEventListener('click', function() {
        const scrollToTop = function() {
            if (document.documentElement.scrollTop > 0) {
                // Scroll progressif en haut de la page
                document.documentElement.scrollBy(0, -50);
                // Rappel récursif pour créer l'effet d'animation douce
                requestAnimationFrame(scrollToTop);
            }
        };
        scrollToTop();
    });
});

const marquee = document.getElementById("scrolling-marquee");

marquee.addEventListener("mouseover", () => {
    marquee.stop();
});

marquee.addEventListener("mouseout", () => {
    marquee.start();
});